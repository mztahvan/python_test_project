import datetime as dt
from datetime import datetime as dt2
from enum import IntEnum


class Day(IntEnum):
    monday = 0,
    tuesday = 1,
    wednesday = 2,
    thursday = 3,
    friday = 4,
    saturday = 5,
    sunday = 6

    def next(self):
        return Day(0 if self == 6 else self + 1)


def seconds_to_12_hour_time(seconds):
    try:
        twenty_four_hour = str(dt.timedelta(seconds=seconds))
        second_to_date = dt2.strptime(twenty_four_hour, "%H:%M:%S")
        postfix = second_to_date.strftime("%p")
        minutes = second_to_date.strftime("%M")
        hours = second_to_date.strftime("%I")
        if hours[0] == '0':
            hours = hours[1:]
        if minutes == '00':
            return '{} {}'.format(hours, postfix)
        return '{}:{} {}'.format(hours, minutes, postfix)
    except:
        return "Invalid data"


def format_week_data(data):
    formatted = ''
    for day, opening_hours in data.items():
        if len(opening_hours) % 2 == 1:
            next_day_str = Day[day].next().name
            if next_day_str in data:
                closing_item = data[next_day_str].pop(0)
                opening_hours.append(closing_item)
        formatted += day.title() + ": " + format_day(opening_hours) + "\n"
    return formatted


def format_day(day_data):
    if len(day_data) == 0:
        return "Closed"
    formatted = ''
    for i in range(0, len(day_data)):
        if day_data[i]["type"] == "open":
            if i > 0:
                formatted += ', '
            formatted += seconds_to_12_hour_time(day_data[i]["value"])
        elif day_data[i]["type"] == "close":
            formatted += ' - ' + seconds_to_12_hour_time(day_data[i]["value"])
    return formatted
