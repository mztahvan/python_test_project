import unittest
from app.openinghours import *


class OpeningHoursTests(unittest.TestCase):
    hours_simple = {
        "monday": [
            {
                "type": "open",
                "value": 32400
            },
            {
                "type": "close",
                "value": 72000
            },
        ],
        "tuesday": [],
        "saturday": [
            {
                "type": "open",
                "value": 1000
            },
            {
                "type": "close",
                "value": 5000
            },
            {
                "type": "open",
                "value": 10000
            },
            {
                "type": "close",
                "value": 30000
            },
        ],
        "sunday": []
    }

    hours_closed_next_day = {
        "sunday": [
            {
                "type": "open",
                "value": 72000
            },
        ],
        "monday": [
            {
                "type": "close",
                "value": 1000
            },
        ]
    }

    def test_seconds_to_12_hour_time(self):
        self.assertEqual("11:59 PM", seconds_to_12_hour_time(86399))
        self.assertEqual("9 AM", seconds_to_12_hour_time(32400))
        self.assertEqual("8 PM", seconds_to_12_hour_time(72000))
        self.assertEqual("Invalid data", seconds_to_12_hour_time(86400))
        self.assertEqual("Invalid data", seconds_to_12_hour_time('hello'))

    def test_format_day_data_case_closed(self):
        self.assertEqual("Closed", format_day(self.hours_simple['tuesday']))

    def test_format_day_data_case_one_open_close(self):
        self.assertEqual("9 AM - 8 PM", format_day(self.hours_simple['monday']))

    def test_format_day_data_case_multiple_open_close(self):
        self.assertEqual("12:16 AM - 1:23 AM, 2:46 AM - 8:20 AM", format_day(self.hours_simple['saturday']))

    def test_forms_one_input_to_human_readable_format(self):
        expected = "Monday: 9 AM - 8 PM\nTuesday: Closed\n" \
                   "Saturday: 12:16 AM - 1:23 AM, 2:46 AM - 8:20 AM\nSunday: Closed\n"
        self.assertEqual(expected, format_week_data(self.hours_simple))

    def test_opening_hours_jumps_to_next_day(self):
        expected = "Sunday: 8 PM - 12:16 AM\nMonday: Closed\n"
        self.assertEqual(expected, format_week_data(self.hours_closed_next_day))


if __name__ == '__main__':
    unittest.main()
