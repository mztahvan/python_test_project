from flask import request
from flask_api import status
from app.openinghours import format_week_data
from app import app


@app.route('/create_opening_hours', methods=['POST'])
def index():
    content = request.json
    try:
        return format_week_data(content)
    except:
        return "Invalid data", status.HTTP_400_BAD_REQUEST
