### Prerequisites

Install python3, pip for python3 and pipenv. These exact versions were used: 

```
➜  python --version
Python 3.7.2
➜  pip --version
pip 18.1
➜ pipenv --version
pipenv, version 2018.11.26
```

When you have python3 and pip for python3, install pipenv by running `pip3 install pipenv`

### Installing

Go to the project folder:

```
cd python_test_project
``` 

Install virtual environment and all required dependencies for running this project:

```
pipenv install
```

## Testing manually

Go to the project folder:

```
cd python_test_project
``` 

Start virtual environment:

```
pipenv shell
```

Start server:

```
FLASK_APP=server.py flask run
```

Open another terminal window, go to python_playground folder and test the application by running:

```
pipenv shell
python client.py
```

You can modify the posted data by modifying the data in client.py and rerunning `python client.py`

## Running the tests

Go to project root folder, start virtual environment by typing `pipenv shell`. Run tests using the following command: `python -m unittest app.tests.openinghourstest`

##Thoughts & Limitations

* The API will return "Invalid data" for all posted JSON data which did not follow the specification, but won't tell where the error was located.
* The API will return restaurant opening times in plain text. It is a bit annoying to programmatically format plain text. Indexing could be done by splitting the text by using newline character **'\n'**, but maybe JSON response with some indexes could be done in the future e.g. {"Monday": "Closed"}
* The API handles currently only one week of opening hours. In case there are use case for getting opening hours for some specific week e.g. week 8, it would be better to integrate and store annual opening hours to the database and create a GET API for getting specific opening hours for a restaurant.
* It would be possible to format opening hours without "type"-key in JSON data by using modulo operation (odd/even check instead of open/close check). It might make the code less readable, but it would save some bandwidth :)

   

## Author

**Matti Tahvanainen** 