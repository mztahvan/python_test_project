import requests

good_data = {
    "monday": [],
    "tuesday": [
        {
            "type": "open",
            "value": 36000
        },
        {
            "type": "close",
            "value": 64800
        }
    ],
    "wednesday": [],
    "thursday": [
        {
            "type": "open",
            "value": 36000
        },
        {
            "type": "close",
            "value": 64800
        }
    ],
    "friday": [
        {
            "type": "open",
            "value": 36000
        }
    ],
    "saturday": [
        {
            "type": "close",
            "value": 3600
        }, {
            "type": "open",
            "value": 36000
        }
    ],
    "sunday": [
        {
            "type": "close",
            "value": 3600
        },
        {
            "type": "open",
            "value": 43200
        },
        {
            "type": "close",
            "value": 75600
        }
    ]
}

poor_data = {
    "ss": [
        {
            "asd": "open",
            "adsdddd": 72000
        },
    ],
    "ddd": [
        {
            "type": "open",
            "asdsa": 22313221
        },
    ]
}

res = requests.post('http://127.0.0.1:5000/create_opening_hours', json=good_data)
if res.ok:
    print(res.text)
else:
    print(res)
